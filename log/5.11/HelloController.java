package com.example.demo;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.repositories.MyDataRepositories;

@Controller
public class HelloController {
	
	@Autowired
	private MyDataRepositories repository;
	
	public ModelAndView index(ModelAndView mav) {
		mav.setViewName("index");
		mav.addObject("msg","this is sample content.");
		
		Iterable<MyData> iter = repository.findAll();
		mav.addObject("data",iter);
		
		return mav;
		
	}
	
}

