package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
	
	
	@RequestMapping("/")   
	public ModelAndView index(	
			ModelAndView mav
			
	) {
		mav.setViewName("index");
		
		List<String[]> list = new ArrayList<>();
		list.add(new String[] {"taro","taro@yamada","090-999-999"});
		list.add(new String[] {"hanako","hanako@flower","080-888-888"});
		list.add(new String[] {"sachiko","sachiko@happy","080-888-888"});
		mav.addObject("data",list);
		
		
		return mav;
	}
}

class DataObject{
	private int id;
	private String name;
	private String value;
	
	public DataObject(int id, String name, String value) {
		super();
		this.id = id;
		this.name = name;
		this.value = value;
	}
	
	//getter setter 
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}

