package com.example.demo;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.repositories.MyDataRepositories;

@Controller
public class HelloController {
	
	@Autowired
	private MyDataRepositories repository;
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView index (
			@ModelAttribute("formModel") MyData mydata,
			ModelAndView mav
			) {
		mav.setViewName("index");
		mav.addObject("msg","this is sample content.");
		
		Iterable<MyData> iter = repository.findAll();
		mav.addObject("datalist",iter);
		
		return mav;
		
	}
	
	@RequestMapping(value="/", method=RequestMethod.POST)
	public ModelAndView form(
			@ModelAttribute("formModel") MyData mydata,
			ModelAndView mav
	) {
		repository.saveAndFlush(mydata);
		return new ModelAndView("redirect:/");
	}
}

