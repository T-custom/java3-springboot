package com.example.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.MyData;

@Repository
public interface MyDataRepositories extends JpaRepository<MyData, Long>{
	

}
